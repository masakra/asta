
package online.ecovolt.asta;

import android.content.Context;
import android.content.Intent;
import org.qtproject.qt5.android.bindings.QtService;

public class AstaServ extends QtService
{
	public static void startAstaServ( Context ctx ) {
		ctx.startService( new Intent( ctx, AstaServ.class ) );
	}
}
