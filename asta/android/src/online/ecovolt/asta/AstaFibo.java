
package online.ecovolt.asta;

public class AstaFibo
{
	public static int fibonacci( int n )
	{
		if ( n < 2 )
			return n;

		return fibonacci( n - 1 ) + fibonacci( n - 2 );
	}
}

