

TARGET = asta
ONFIG += c++11
QT += widgets qml quick

DEFINES += DEVEL

android {
	QT += androidextras
}

HEADERS += src/QmlApplication.h

SOURCES += src/main.cpp \
		   src/QmlApplication.cpp

RESOURCES += qml.qrc

DISTFILES += \
    android/AndroidManifest.xml \
    android/gradle/wrapper/gradle-wrapper.jar \
    android/gradlew \
    android/res/values/libs.xml \
    android/build.gradle \
    android/gradle/wrapper/gradle-wrapper.properties \
    android/gradlew.bat

ANDROID_PACKAGE_SOURCE_DIR = $$PWD/android
