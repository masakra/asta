
import QtQuick 2.7
import QtQuick.Controls 2.0

ApplicationWindow {
	id: window
	objectName: "MainWindow"
	visible: true
	width: 480
	height: 640

	signal toaster()

	signal startService()

	Button {
		x: 20
		y: 20
		width: 200
		height: 40
		text: "Toast test"

		onClicked: window.toaster()
	}

	Button {
		x: 20
		y: 80
		width: 200
		height: 40
		text: "Start service"

		onClicked: window.startService()
	}
}

