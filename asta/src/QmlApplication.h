
#include <QQmlApplicationEngine>

class QmlApplication : public QQmlApplicationEngine
{
	Q_OBJECT

	private:

		enum Duration {
			Short = 0,
			Long = 1
		};

		void showToast( const QString & message, Duration duration = Long );

		int fibonacci( int n );

	private Q_SLOTS:
		void toaster();
		void startService();


	public:
		QmlApplication( QObject * parent = nullptr );

		QmlApplication( const QUrl & url, QObject * parent = nullptr );

		void load( const QUrl & url );
};

