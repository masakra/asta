
#include "QmlApplication.h"

#ifdef Q_OS_ANDROID
#include <QtAndroid>
#endif

#include <QDebug>

QmlApplication::QmlApplication( QObject * parent )
	: QQmlApplicationEngine( parent )
{
}

QmlApplication::QmlApplication( const QUrl & url, QObject * parent )
	: QQmlApplicationEngine( url, parent )
{
}

void
QmlApplication::toaster()		// private slot
{
	showToast("toaster");

	qDebug() << "Fibonacci ==" << fibonacci( 7 );
}

void
QmlApplication::startService()
{
	qDebug() << "Start service !";
#ifdef Q_OS_ANDROID
	QAndroidJniObject::callStaticMethod< void >("online/ecovolt/asta/AstaServ",
			"startAstaServ",
			"(Landroid/content/Context;)V",
			QtAndroid::androidActivity().object() );
#endif
}

void
QmlApplication::load( const QUrl & url )
{
	QQmlApplicationEngine::load( url );

	const QList< QObject * > roots = rootObjects();

	QObject * rootObject = roots.isEmpty() ? nullptr : roots.first();

	if ( ! rootObject )
	{
		qDebug() << "NO root object found.";
		return;
	}

	connect( rootObject, SIGNAL( toaster() ), SLOT( toaster() ) );

	connect( rootObject, SIGNAL( startService() ), SLOT( startService() ) );
}


void
QmlApplication::showToast( const QString & message, Duration duration )
{
#ifdef Q_OS_ANDROID
	QtAndroid::runOnAndroidThread( [ message, duration ]
			{
				auto javaString = QAndroidJniObject::fromString( message );
				auto toast = QAndroidJniObject::callStaticObjectMethod("android/widget/Toast", "makeText",
						"(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;",
						QtAndroid::androidActivity().object(),
						javaString.object(),
						jint( duration ) );

				toast.callMethod< void >("show");
			} );
#else
	qDebug() << "showToast(" << message << ")" << duration;
#endif
}

int
QmlApplication::fibonacci( int n )
{
#ifdef Q_OS_ANDROID
	return QAndroidJniObject::callStaticMethod< jint >(
			"online/ecovolt/asta/AstaFibo",
			"fibonacci",
			"(I)I",
			n );
#endif
}

