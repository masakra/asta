
#include <QGuiApplication>

#ifdef Q_OS_ANDROID
#include <QtAndroid>
#endif

#include "QmlApplication.h"

int
main( int argc, char ** argv )
{
	QGuiApplication app( argc, argv );

	QmlApplication qmlApp;
	qmlApp.load( QUrl( QLatin1String("qrc:/main.qml") ) );
#ifdef Q_OS_ANDROID
	QtAndroid::hideSplashScreen();
#endif
	return app.exec();
}

