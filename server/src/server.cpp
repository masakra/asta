
#include <QCoreApplication>
//#include <QtAndroid>
//#include <QDebug>

/*
enum Duration {
	Short = 0,
	Long = 1
};

void
showToast( const QString & message, Duration duration = Long )
{
#ifdef Q_OS_ANDROID
	QtAndroid::runOnAndroidThread( [ message, duration ]
			{
				auto javaString = QAndroidJniObject::fromString( message );
				auto toast = QAndroidJniObject::callStaticObjectMethod("android/widget/Toast", "makeText",
						"(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;",
						QtAndroid::androidActivity().object(),
						javaString.object(),
						jint( duration ) );

				toast.callMethod< void >("show");
			} );
#else
	qDebug() << "showToast(" << message << ")" << duration;
#endif
}
*/



int
main( int argc, char ** argv )
{
	QCoreApplication app( argc, argv );

	//showToast("SERVICE IS RUNNING");

	return app.exec();
}

